function Resources() {



    return (
        <div className="App-header">
            <h1 className="welcome mb-5 Chat-text">Resources</h1>
            <p className="help font-bold">Need help immediately? Call 911 or the Veterans Crisis Line at 1-800-273-8255 and Press 1, or text 838255.</p>
        <div className="card-container">
            <div className="card">
                <h2 className="resource-header">Mental Health</h2>
                <ul className="text-sm">
                    <li className="hover:text-blue-500"><a href="https://www.veteranscrisisline.net/">https://www.veteranscrisisline.net/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.samhsa.gov/find-help/national-helpline">https://www.samhsa.gov/find-help/national-helpline</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.health.mil/Military-Health-Topics/Centers-of-Excellence/Traumatic-Brain-Injury-Center-of-Excellence/A-Head-for-the-Future">https://www.health.mil/Military-Health-Topics/Centers-of-Excellence/A-Head-for-the-Future</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.militaryonesource.mil/resources/mobile-apps/de-stress-and-relax-with-chill-drills-by-military-onesource">Chill Drills</a></li>
                    <li className="hover:text-blue-500"><a href="https://findtreatment.gov/">https://findtreatment.gov</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.veterantraining.va.gov/movingforward/">https://www.veterantraining.va.gov/movingforward/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.nami.org/Your-Journey/Veterans-Active-Duty">https://www.nami.org/Your-Journey/Veterans-Active-Duty</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.taps.org/requestapeermentor">https://www.taps.org/requestapeermentor</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.maketheconnection.net/resources/what-is-treatment/">https://www.maketheconnection.net/resources/what-is-treatment/</a></li>
                    <li className="hover:text-blue-500"><a href="https://dpt2.samhsa.gov/treatment/directory.aspx">https://dpt2.samhsa.gov/treatment/directory.aspx</a></li>
                </ul>
            </div>
            <div className="card">
                <h2 className="resource-header">Veteran's Affairs</h2>
                <ul className="text-sm">
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/health-care">https://www.va.gov/health-care</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/disability">https://www.va.gov/disability</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/education">https://www.va.gov/education</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/housing-assistance">https://www.va.gov/housing-assistance</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/family-member-benefits">https://www.va.gov/family-member-benefits</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/service-member-benefits">https://www.va.gov/service-member-benefits</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/careers-employment">https://www.va.gov/careers-employment</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/records">https://www.va.gov/records</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/pension">https://www.va.gov/pension</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/burials-memorials">https://www.va.gov/burials-memorials</a></li>
                </ul>
            </div>
            <div className="card">
                <h2 className="resource-header">Education</h2>
                <ul className="text-sm">
                    <li className="hover:text-blue-500"><a href="https://www.galvanize.com/military/veterans/vet-tec-funding/">https://www.galvanize.com/military/veterans/vet-tec-funding/</a></li>
                    <li className="hover:text-blue-500"><a href="https://vetsedsuccess.org/">https://vetsedsuccess.org/</a></li>
                    <li className="hover:text-blue-500"><a href="https://studentveterans.org/">https://studentveterans.org/</a></li>
                    <li className="hover:text-blue-500"><a href="https://militaryconnection.com/scholarships/">https://militaryconnection.com/scholarships/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.accreditedschoolsonline.org/resources/higher-education-for-military-veterans/">https://www.accreditedschoolsonline.org/resources/higher-education-for-military-veterans/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.ed.gov/veterans-and-military-families/information">https://www.ed.gov/veterans-and-military-families/information</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.va.gov/education/about-gi-bill-benefits/how-to-use-benefits/vettec-high-tech-program/">https://www.va.gov/education/about-gi-bill-benefits/how-to-use-benefits/vettec-high-tech-program/</a></li>
                    <li className="hover:text-blue-500"><a href="https://inquiry.vba.va.gov/weamspub/buildSearchInstitutionCriteria.do">https://inquiry.vba.va.gov/weamspub/buildSearchInstitutionCriteria.do</a></li>


                </ul>
            </div>
            <div className="card">
                <h2 className="resource-header">Financial Support</h2>
                <ul className="text-sm">
                    <li className="hover:text-blue-500"><a href="https://www.consumerfinance.gov/paying-for-college/">https://www.consumerfinance.gov/paying-for-college/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.armyemergencyrelief.org/">https://www.armyemergencyrelief.org/</a></li>
                    <li className="hover:text-blue-500"><a href="https://operationfamilyfund.org/">https://operationfamilyfund.org/</a></li>
                    <li className="hover:text-blue-500"><a href="https://saluteheroes.org/get-help/emergency-financial-aid/">https://saluteheroes.org/get-help/emergency-financial-aid/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.legion.org/financialassistance/">https://www.legion.org/financialassistance/</a></li>
                    <li className="hover:text-blue-500"><a href="http://www.naavets.org/programs/">http://www.naavets.org/programs/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.operationfirstresponse.org/military-family-assistance-program/">https://www.operationfirstresponse.org/military-family-assistance-program/</a></li>
                    <li className="hover:text-blue-500"><a href="https://semperfifund.org/get-assistance/">https://semperfifund.org/get-assistance/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.cfvfunited.com/programs/">https://www.cfvfunited.com/programs/</a></li>
                    <li className="hover:text-blue-500"><a href="http://www.veteransinc.org/services/housing-programs/">http://www.veteransinc.org/services/housing-programs/</a></li>
                </ul>
            </div>
            <div className="card">
                <h2 className="resource-header">Employment</h2>
                <ul className="text-sm">
                    <li className="hover:text-blue-500"><a href="http://www.dol.gov/vets/">http://www.dol.gov/vets/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.oriontalent.com/solutions/military-talent/">https://www.oriontalent.com/solutions/military-talent/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.dol.gov/agencies/vets/veterans/veterans-employment-services">https://www.dol.gov/agencies/vets/veterans/veterans-employment-services</a></li>
                    <li className="hover:text-blue-500"><a href="https://kcc.ky.gov/veterans/Pages/default.aspx">https://kcc.ky.gov/veterans/Pages/default.aspx</a></li>
                    <li className="hover:text-blue-500"><a href="https://saluteheroes.org/get-help/national-employment-resources/">https://saluteheroes.org/get-help/national-employment-resources/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.usajobs.gov/Help/working-in-government/unique-hiring-paths/veterans/">https://www.usajobs.gov/Help/working-in-government/unique-hiring-paths/veterans/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.transportation.gov/veteranstransportationcareers">https://www.transportation.gov/veteranstransportationcareers</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.vets.gov/employment/job-seekers/federal-employment/">https://www.vets.gov/employment/job-seekers/federal-employment/</a></li>
                    <li className="hover:text-blue-500"><a href="https://www.opm.gov/policy-data-oversight/veterans-services/">https://www.opm.gov/policy-data-oversight/veterans-services/</a></li>
                    <li className="hover:text-blue-500"><a href="https://teamster.org/tmap">https://teamster.org/tmap</a></li>
                </ul>
            </div>
            <div className="card">
                <h2 className="resource-header">Partner Links</h2>
                <ul className="text-sm">
                    <li>Coming Soon!</li>
                </ul>
            </div>
        </div>
    </div>
    );
}

export default Resources;
